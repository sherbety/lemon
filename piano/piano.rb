# piano.rb
require 'sinatra'

=begin
The piano class
@author = David Sherratt

When the server is running, can receive two inputs 'melody' and 'shift'. The first is the melody, a 
collection of notes that can be played on a piano, and shift should be an integer value. The server will
then output the corresponding melody where each note in the melody has been 'shifted' by the shift value.
=end
class Piano < Sinatra::Base
  # list of keys in an octave 
  @@keys = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]  

  KEYLIMIT = 88
  
  #Function getKey
  #Returns string "invalid" if input is out of bounds
  #Returns string note corresponding to the input otherwise
  def self.getKey(index_i) 
    #Checks input is in range of possible notes
	if index_i >= KEYLIMIT || index_i < 0
      return "invalid"
    end
	#If 0 <= input <= 2, we returns the known values.
	#Makes it easier to reason since our array starts on note 'C'
	#Alternatively we can reorder the array, but then will need to pay attention to when we increase the octave
    if index_i == 0
      return "A0"
    elsif index_i == 1
      return "A#0"
    elsif index_i == 2
      return "B0"
    end
	##decrease the input to ignore the first 3 cases
    index_i = index_i - 3
	#determine which note it should be from input
    letter_i = index_i % @@keys.size
    letter_s = @@keys[letter_i]
    #determine which octave it should be from input
    arrayIndex_i = (index_i / @@keys.size) + 1
    return letter_s + arrayIndex_i.to_s
  end  
  
  #Loops through all possible key indexes, if one mathes we return it, otherwise return string invalid.
  def self.getIndex(index_s)
    KEYLIMIT .times { |i| return i if getKey(i) == index_s }
    return "invalid"
  end
  
  #If input can be read as an integer, returns the integer value. Otherwise returns string ERROR.
  def self.getInteger(value)
    return Integer(value) rescue "ERROR"
  end

  #Checks each note in melody is valid. If at least one note is invalid then the melody is invalid.
  def self.isMelody(melody)
    melody.each { |n| return false if Piano.getIndex(n) == "invalid" }
	return true;
  end

  #Function that takes in an individual note and shifts it by a value.
  #This is the function that is mapped onto the melody.
  #This function returns the index corresponding to the note, and not
  #the note itself.
  def self.shiftNote(note, shift)
    noteValue = getIndex(note)
	#If the note is not valid, we cannot shift it, so we return string invalid.
	if noteValue.to_s == "invalid"
	  return "invalid"
	end
	#If the shift value is not a number, we return string ERROR.
	if getInteger(shift) == "ERROR"
	 return "ERROR"
	end
    shift_i = getInteger(shift)
	#Checks the new note value is at within range, so that it is still a note after shifting.
    if ((noteValue + shift_i) >= KEYLIMIT || (noteValue + shift_i) < 0)
      return "OUTOFRANGE"
    else 
      return noteValue + shift_i
    end
  end
  
  def self.shiftMelody(melody, shiftValue)
    melody_a = melody.split(/,/)
	shift_i = shiftValue.to_i
	if (getInteger(shiftValue) == "ERROR")
	  return "invalid value for shift: '" + shiftValue.to_s + "'"
	elsif (!isMelody(melody_a))
	  return "invalid value for melody: '" + melody + "'" 
	else
	  #maps the shift function onto array of notes
	  melody_a = melody_a.map { |n| Piano.shiftNote(n, shift_i) }
	  #The folloiwng loop inspects each element to see if
	  #any of them were made out of range, resulting in an
	  #melody out of the trange of a piano keyboard.
	  for i in (0..melody_a.size)
        if melody_a[i].to_s == "OUTOFRANGE"
		  return "transposed melody out of range"
		end
      end	  
	  #the array of melodies are integers, so we map the getKey function to make them notes.
	  melody_a = melody_a.map { |n| Piano.getKey(n) }
	  #Returns list as a string
	  return melody_a.join(',') 
	end
  end

  #When contacted on the server, looks for a melody and shift parameter.
  get '/' do
    if params[:melody] == nil || params[:shift] == nil
	  "Melody or Shift value not entered"
	else
	  Piano.shiftMelody( params[:melody], params[:shift] )
	end
  end

end