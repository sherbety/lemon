#pianist.rb

ENV['APP_ENV'] = 'test'

require "spec_helper"
require_relative 'piano'
require 'rspec'
require 'rack/test'

RSpec.configure do |conf|
  conf.include Rack::Test::Methods
end

RSpec.describe Piano do

  include Rack::Test::Methods
  
  context "getKey: Typical Cases" do
    it "should return an key when we call the getKey method" do
	  expect(Piano.getKey(20)).to eq("F2")
    end
	it "should return an key when we call the getKey method" do
	  expect(Piano.getKey(50)).to eq("B4")
    end
  end
  
  context "getKey: Extreme Cases" do
    it "should return an key when we call the getKey method" do
	  expect(Piano.getKey(0)).to eq("A0")
    end
	it "should return an key when we call the getKey method" do
	  expect(Piano.getKey(87)).to eq("C8")
    end
  end
  
  context "getKey: Erroneous Cases" do
    it "should return an error when we call the getKey method" do
	  expect(Piano.getKey(-1)).to eq("invalid")
    end
	it "should return an error when we call the getKey method" do
	  expect(Piano.getKey(88)).to eq("invalid")
    end
  end
  
  context "getIndex: Typical Cases" do
    it "should return an index when we call the getIndex method" do
	  expect(Piano.getIndex("F2")).to eq(20)
    end
	it "should return an index when we call the getIndex method" do
	  expect(Piano.getIndex("B4")).to eq(50)
    end
  end
  
  context "getIndex: Extreme Cases" do
    it "should return an index when we call the getIndex method" do
	  expect(Piano.getIndex("A0")).to eq(0)
    end
	it "should return an index when we call the getIndex method" do
	  expect(Piano.getIndex("C8")).to eq(87)
    end
  end
  
  context "getIndex: Erroneous Cases" do
    it "should return an index when we call the getIndex method" do
	  expect(Piano.getIndex("K4")).to eq("invalid")
    end
	it "should return an index when we call the getIndex method" do
	  expect(Piano.getIndex("hello")).to eq("invalid")
    end
  end
  
  context "getInteger: Erroneous Cases" do
    it "should return an ERROR when we call the getInteger method" do
	  expect(Piano.getInteger("7dd")).to eq("ERROR")
    end
	it "should return an ERROR when we call the getInteger method" do
	  expect(Piano.getInteger("hello")).to eq("ERROR")
    end
  end
  
  context "getInteger: Typical Cases" do
    it "should return an integer when we call the getInteger method" do
	  expect(Piano.getInteger("7")).to eq(7)
    end
	it "should return an integer when we call the getInteger method" do
	  expect(Piano.getInteger("0")).to eq(0)
    end
	it "should return an integer when we call the getInteger method" do
	  expect(Piano.getInteger("-7")).to eq(-7)
    end
  end
  context "getInteger: Erroneous Cases" do
    it "should return an ERROR when we call the getInteger method" do
	  expect(Piano.getInteger("7dd")).to eq("ERROR")
    end
	it "should return an ERROR when we call the getInteger method" do
	  expect(Piano.getInteger("hello")).to eq("ERROR")
    end
  end
  
  context "isMelody: Typical Cases" do
    it "should return true when we call the isMelody method" do
	  melody = ["B4","B4","C5","D5","D5","C5","B4","A4"]
	  expect(Piano.isMelody(melody)).to be true
    end
  end
  context "isMelody: Extreme Cases" do
    it "should return true when we call the isMelody method with empty list" do
	  melody = []
	  expect(Piano.isMelody(melody)).to be true
    end
  end
  context "isMelody: Erroneous Cases" do
    it "should return false when we call the isMelody method" do
	  melody = ["B4","K4","C5","D5","D5","C5","B4","A4"]
	  expect(Piano.isMelody(melody)).to be false
    end
  end
  
  context "isMelody: Typical Cases" do
    it "should return true when we call the isMelody method" do
	  melody = ["B4","B4","C5","D5","D5","C5","B4","A4"]
	  expect(Piano.isMelody(melody)).to be true
    end
  end
  
  context "isMelody: Extreme Cases" do
    it "should return true when we call the isMelody method with empty list" do
	  melody = []
	  expect(Piano.isMelody(melody)).to be true
    end
  end
  
  context "isMelody: Erroneous Cases" do
    it "should return false when we call the isMelody method" do
	  melody = ["B4","K4","C5","D5","D5","C5","B4","A4"]
	  expect(Piano.isMelody(melody)).to be false
    end
  end
  
  context "shiftNote: Typical Cases" do
    it "should return new note when we call the shiftNote method" do
	  expect(Piano.shiftNote("E4", 4)).to eq 47
    end
	it "should return new note when we call the shiftNote method" do
	  expect(Piano.shiftNote("E4", -4)).to eq 39
    end
  end
  
  context "shiftNote: Extreme Cases" do
    it "should return new note when we call the shiftNote method" do
	  expect(Piano.shiftNote("A0", 5)).to eq 5
    end
	it "should return new note when we call the shiftNote method" do
	  expect(Piano.shiftNote("C8", -5)).to eq 82
    end
	it "should return new note when we call the shiftNote method" do
	  expect(Piano.shiftNote("D1", -5)).to eq 0
    end
	it "should return new note when we call the shiftNote method" do
	  expect(Piano.shiftNote("G7", 5)).to eq 87
    end
  end
  
  context "shiftNote: Erroneous Cases" do
    it "should return an error message when we call the shiftNote method" do
	  expect(Piano.shiftNote("A0", -1)).to eq "OUTOFRANGE"
    end
    it "should return an error message we call the shiftNote method" do
	  expect(Piano.shiftNote("C8", 1)).to eq "OUTOFRANGE"
    end
	it "should return an error message we call the shiftNote method" do
	  expect(Piano.shiftNote("K4", 1)).to eq "invalid"
    end
	it "should return an error message we call the shiftNote method" do
	  expect(Piano.shiftNote("C4", "hello")).to eq "ERROR"
    end
  end
  
  context "shiftMelody: Typical Cases" do
    it "should return an melody when we call the shiftNote method" do
	  melody = "B4,B4,C5,D5,D5,C5,B4,A4"
	  expect(Piano.shiftMelody(melody, 4)).to eq "D#5,D#5,E5,F#5,F#5,E5,D#5,C#5" 
    end
    it "should return an melody when we call the shiftNote method" do
	  melody = "B4,B4,C5,D5,D5,C5,B4,A4"
	  expect(Piano.shiftMelody(melody, -4)).to eq "G4,G4,G#4,A#4,A#4,G#4,G4,F4" 
    end
  end
  
  context "shiftMelody: Extreme Cases" do
    it "should return an melody when we call the shiftNote method" do
	  melody = "A0,A0,A0"
	  expect(Piano.shiftMelody(melody, 87)).to eq "C8,C8,C8" 
    end
	it "should return an melody when we call the shiftNote method" do
	  melody = "C8,C8,C8"
	  expect(Piano.shiftMelody(melody, -87)).to eq "A0,A0,A0" 
    end
	it "should return an melody when we call the shiftNote method" do
	  melody = ""
	  expect(Piano.shiftMelody(melody, 4)).to eq "" 
    end
  end
  
  context "shiftMelody: Erroneous Cases" do
    it "should return an error message when we call the shiftNote method" do
	  melody = "B4,K4,C5,D5,D5,C5,B4,A4"
	  expect(Piano.shiftMelody(melody, 4)).to eq "invalid value for melody: 'B4,K4,C5,D5,D5,C5,B4,A4'" 
    end
	it "should return an error message when we call the shiftNote method" do
	  melody = "B4,B4,C5,D5,D5,C5,B4,A4"
	  expect(Piano.shiftMelody(melody, "hello")).to eq "invalid value for shift: 'hello'" 
    end
	it "should return an error message when we call the shiftNote method" do
	  melody = "B4,B4,C5,D5,D5,C5,B4,A4"
	  expect(Piano.shiftMelody(melody, 400)).to eq "transposed melody out of range" 
    end
	it "should return an error message when we call the shiftNote method" do
	  melody = "B4,B4,C5,D5,D5,C5,B4,A4"
	  expect(Piano.shiftMelody(melody, -400)).to eq "transposed melody out of range" 
    end
  end
  
  #Don't know why this is needed, but otherwise its an unassigned method.
  def app
    Piano
  end
  
  context "WebServer: Typical Cases" do
    it "should return new melody" do
	  get '/?melody=B4&shift=4'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("D#5")
    end
	it "should return new melody" do
	  get '/?melody=B4,B4,C5,D5,D5,C5,B4,A4&shift=4'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("D#5,D#5,E5,F#5,F#5,E5,D#5,C#5")
    end
	it "should return new melody" do
	  get '/?melody=B4,B4,C5,D5,D5,C5,B4,A4&shift=-4'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("G4,G4,G#4,A#4,A#4,G#4,G4,F4")
    end
  end
  
  context "WebServer: Extreme Cases" do
    it "should return new melody" do
	  get '/?melody=A0,A0,A0&shift=87'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("C8,C8,C8")
    end
	it "should return new melody" do
	  get '/?melody=C8,C8,C8&shift=-87'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("A0,A0,A0")
    end
	it "should return new melody" do
	  get '/?melody=&shift=-4'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("")
    end
  end
  
    context "WebServer: Erroneous Cases" do
    it "should return an error message" do
	  get '/?melody=B4,K4,C5,D5,D5,C5,B4,A4&shift=87'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("invalid value for melody: 'B4,K4,C5,D5,D5,C5,B4,A4'")
    end
	it "should return an error message" do
	  get '/?melody=B4,B4,C5,D5,D5,C5,B4,A4&shift=hello'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("invalid value for shift: 'hello'")
    end
	it "should return an error message" do
	  get '/?melody=B4,B4,C5,D5,D5,C5,B4,A4&shift=400'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("transposed melody out of range")
    end
	it "should return an error message" do
	  get '/?melody=B4,B4,C5,D5,D5,C5,B4,A4&shift=-400'
	  expect(last_response).to be_ok
	  expect(last_response.body).to eq ("transposed melody out of range")
    end
  end
  
end